require "erubis"
require "r00lz/file_model"

module R00lz
  class Controller
    include R00lz::Model

    attr_reader :env

    def initialize(env)
      @env = env
      @routing_params = {}
    end

    def request
      @request ||= Rack::Request.new(@env)
    end

    def params
      request.params.merge @routing_params
    end

    def render(view_name)
      template = File.read "app/views/#{controller_name}/#{view_name}.html.erb"
      view = View.new
      view.set_vars instance_hash
      view.evaluate template
    end

    def controller_name
      klass = self.class
      klass = klass.to_s.gsub /Controller$/, ""
      R00lz.to_underscore klass
    end

    def instance_hash
      hash = {}
      instance_variables.each do |i|
        hash[i] = instance_variable_get i
      end
      hash
    end

    def self.action(action, routing_params = {})
      proc { |e| self.new(e).dispatch(action, routing_params) }
    end

    def dispatch(action, routing_params = {})
      @routing_params = routing_params
      text = self.send(action)
      response = get_response
      if response
        [response.status, response.headers, [response.body].flatten]
      else
        [200, {"Content-Type" => "text/html"}, [text].flatten]
      end
    end

    def get_response
      @response
    end

    def response(text, status = 200, headers = {})
      raise "Already responded!" if @response

      a = [text].flatten
      @response = Rack::Response.new(a, status, headers)
    end
  end
end
