require "multi_json"

module R00lz
  module Model
    class FileModel
      def initialize(filename)
        @filename = filename

        # If filename is "dir/37.json", @id is 37
        basename = File.split(filename)[-1]
        @id = File.basename(basename, ".json").to_i

        obj = File.read(filename)
        @hash = MultiJson.load(obj)
      end

      def [](name)
        @hash[name.to_s]
      end

      def []=(name, value)
        @hash[name.to_s] = value
      end

      def self.find(id)
        FileModel.new("db/quotes/#{id}.json")
      rescue
        nil
      end

      def self.all
        files = Dir["db/quotes/*.json"]
        files.map { |f| FileModel.new f }
      end

      def self.create(attrs)
        hash = {}
        hash["submitter"] = attrs["submitter"] || ""
        hash["quote"] = attrs["quote"] || ""
        hash["attribution"] = attrs["attribution"] || ""

        files = Dir["db/quotes/*.json"]
        names = files.map { |f| File.split(f)[-1] }
        highest = names.map { |b| b.to_i }.max
        id = highest + 1

        File.open("db/quotes/#{id}.json", "w") do |f|
          f.write <<~TEMPLATE
            {
              "submitter": "#{hash["submitter"]}",
              "quote": "#{hash["quote"]}",
              "attribution": "#{hash["attribution"]}"
            }
          TEMPLATE
        end

        FileModel.new "db/quotes/#{id}.json"
      end

      def save
        File.open(@filename, "w") do |f|
          f.write <<~TEMPLATE
            {
              "submitter": "#{@hash["submitter"]}",
              "quote": "#{@hash["quote"]}",
              "attribution": "#{@hash["attribution"]}"
            }
          TEMPLATE
        end
      end

      def self.find_all_by_attrib(attrib, value)
        id = 1
        results = []
        loop do
          model = FileModel.find(id)
          return results unless model

          results.push(model) if model[attrib] == value
          id += 1
        end
      end

      def self.method_missing(method, *args)
        if method.to_s[0..11] == "find_all_by_"
          attrib = method.to_s[12..-1]
          return find_all_by_attrib attrib, args[0]
        end
      end
    end
  end
end
