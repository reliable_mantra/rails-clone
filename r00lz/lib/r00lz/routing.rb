module R00lz
  class RouteObject
    def initialize
      @rules = []
    end

    def match(url, *args)
      options = {}
      options = args.pop if args[-1].is_a?(Hash)
      options[:default] ||= {}

      dest = nil
      dest = args.pop unless args.empty?
      raise "Too many args!" unless args.empty?

      parts = url.split("/")
      parts.reject!(&:empty?)

      vars = []
      regexp_parts = parts.map do |part|
        case part[0]
        when ":"
          vars << part[1..]
          "([a-zA-Z0-9]+)"
        when "*"
          vars << part[1..]
          "(.*)"
        else
          part
        end
      end

      regexp = regexp_parts.join("/")
      @rules.push(
        {
          regexp: Regexp.new("^/#{regexp}$"),
          vars: vars,
          dest: dest,
          options: options
        }
      )
    end

    def check_url(url)
      @rules.each do |rule|
        match = rule[:regexp].match(url)

        if match
          options = rule[:options]
          params = options[:default].dup
          rule[:vars].each_with_index do |v, i|
            params[v] = match.captures[i]
          end
          if rule[:dest]
            return get_dest(rule[:dest], params)
          else
            controller = params["controller"]
            action = params["action"]
            return get_dest("#{controller}" + "##{action}", params)
          end
        end
      end

      nil
    end

    def get_dest(dest, routing_params = {})
      return dest if dest.respond_to?(:call)

      if dest =~ /^([^#]+)#([^#]+)$/
        name = $1.capitalize
        con = Object.const_get("#{name}Controller")
        return con.action($2, routing_params)
      end
      raise "No destination: #{dest.inspect}!"
    end
  end

  class Application
    def get_controller_and_action(env)
      _, controller, action, after = env["PATH_INFO"].split("/", 4)
      controller = "#{controller.capitalize}Controller"
      [Object.const_get(controller), action]
    end

    def route(&block)
      @route_obj ||= RouteObject.new
      @route_obj.instance_eval(&block)
    end

    def get_rack_app(env)
      raise "No routes!" unless @route_obj

      @route_obj.check_url env["PATH_INFO"]
    end
  end
end
