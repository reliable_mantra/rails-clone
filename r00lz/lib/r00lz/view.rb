module R00lz
  class View
    def set_vars(instance_vars)
      instance_vars.each do |name, value|
        instance_variable_set(name, value)
      end
    end

    def evaluate(template)
      eruby = Erubis::Eruby.new(template)
      # Locals are in addition to
      # instance variables, if any
      eval eruby.src
    end

    # View helper methods
    def h(str)
      CGI.escape str
    end
  end
end
