require "erb"
require_relative "r00lz/version"
require_relative "r00lz/dependencies"
require_relative "r00lz/util"
require_relative "r00lz/routing"
require_relative "r00lz/view"
require_relative "r00lz/controller"
require_relative "r00lz/file_model"

require_relative "r00lz/array"

module R00lz
  class Error < StandardError; end

  class Application
    def call(env)
      `echo debug > debug.txt`
      # Don't parse the route here,
      # use a new method we'll write.
      rack_app = get_rack_app(env)
      rack_app.call(env)
    end
  end
end
