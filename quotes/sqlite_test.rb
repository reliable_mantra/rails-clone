require 'sqlite3'
require 'r00lz/sqlite_model'

class MyTable < R00lz::Model::SQLite; end
STDERR.puts MyTable.schema.inspect

# Create row
my_table = MyTable.create 'title' => 'I saw it again!'
my_table['title'] = 'I really did!'
my_table.save!

my_table_2 = MyTable.find my_table['id']

puts "Title: #{my_table_2['title']}"
