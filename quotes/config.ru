require_relative 'config/application'

app = Quotes::Application.new

use Rack::ContentType

app.route do
  match '', 'quotes#index'
  match ':controller/:action'
  match 'sub-app', (proc { [200, {}, ['Hello, sub-app!']] })

  # default routes
  match ':controller/:id/:action'
  match ':controller/:id', default: { 'action' => 'show' }
  match ':controller', default: { 'action' => 'index' }
end

run app
