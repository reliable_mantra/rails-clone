require 'r00lz'

$LOAD_PATH << File.join(File.dirname(__FILE__), '..', 'app', 'controllers')

module Quotes
  class Application < R00lz::Application
  end
end
