class ExampleController < R00lz::Controller
  def quote
    "How you doin'?"
  end

  def rendering
    @noun = 'a BlendTec blender'
    @another_noun = 'a mutton, lettuce and tomato sandwich'
    render :rendering
  end

  def shakes
    @noun = :winking
    render :shakes
  end

  def trick
    n = params['card'] || 'Queen'
    "Your card: the #{n} of spades!"
  end

  def sum
    a = params['a']
    b = params['b']
    a && b ? (a.to_i + b.to_i).to_s : 'You need to set a and b query params to numbers you want to sum'
  end

  def debug
    "\n<pre>\n#{JSON.pretty_generate(env)}\n</pre>"
  end

  def exception
    raise "It's a bad one!"
  end
end
