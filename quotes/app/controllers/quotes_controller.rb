class QuotesController < R00lz::Controller
  def index
    @quotes = FileModel.all
    render :index
  end

  def new
    attrs = {
      'submitter' => 'web user',
      'quote' => 'A picture is worth one k pixels',
      'attribution' => 'Me'
    }
    @quote = FileModel.create attrs
    render :quote
  end

  def show
    @quote = FileModel.find(params['quote'] || 1)
    @user_agent = request.user_agent
    render :quote
  end

  def update
    raise 'Only POST to this route!' unless env['REQUEST_METHOD'] == 'POST'

    body = env['rack.input'].read
    astr = body.split('&')
    params = {}
    astr.each do |a|
      name, val = a.split '='
      params[name] = val
    end

    @quote = FileModel.find(params['id'].to_i)
    @quote['submitter'] = params['submitter']
    @quote.save

    render :quote
  end
end
